import { from } from 'rxjs';
import { fromEvent } from 'rxjs';
// Build a Mortgage Claculator using Rxjs and calculateMortgage method
import { calculateMortgage } from './calculate';

const loanAmount = document.querySelector('#loanAmount') as HTMLInputElement;
const loanInterest = document.querySelector('#loanInterest') as HTMLInputElement;
const loanLength = document.querySelector('#loanLength') as HTMLSelectElement;
const result = document.querySelector('#result') as HTMLDivElement;


fromEvent(loanAmount, 'input').subscribe(() => {
  let mortgage = calculateMortgage(loanAmount.value, loanInterest.value || 1, loanLength.value);
  result.textContent = mortgage;
});
fromEvent(loanInterest, 'input').subscribe(() => {
  let mortgage = calculateMortgage(loanAmount.value, loanInterest.value || 1, loanLength.value);
  result.textContent = mortgage;
});
fromEvent(loanLength, 'change').subscribe(() => {
  let mortgage = calculateMortgage(loanAmount.value, loanInterest.value || 1, loanLength.value);
  console.log(loanLength.value);
  result.textContent = mortgage;
});
